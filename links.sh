echo "sddm service"
ln -v -s /usr/lib/systemd/system/sddm.service display-manager.service

echo "networkmanager"
ln -v -s /usr/lib/systemd/system/NetworkManager.service multi-user.target.wants/NetworkManager.service
ln -v -s /usr/lib/systemd/system/NetworkManager-dispatcher.service dbus-org.freedesktop.nm-dispatcher.service
ln -v -s /usr/lib/systemd/system/NetworkManager-wait-online.service network-online.target.wants/NetworkManager-wait-online.service

echo "bluetooth"
ln -v -s /usr/lib/systemd/system/bluetooth.service dbus-org.bluez.service
mkdir -v bluetooth.target.wants
ln -v -s /usr/lib/systemd/system/bluetooth.service bluetooth.target.wants/bluetooth.service

echo "cups"
mkdir -v printer.target.wants
ln -v -s /usr/lib/systemd/system/cups.service printer.target.wants/cups.service
ln -v -s /usr/lib/systemd/system/cups.service multi-user.target.wants/cups.service
ln -v -s /usr/lib/systemd/system/cups.socket sockets.target.wants/cups.socket
ln -v -s /usr/lib/systemd/system/cups.path multi-user.target.wants/cups.path

echo "paccache"
mkdir -v timers.target.wants
ln -v -s /usr/lib/systemd/system/paccache.timer timers.target.wants/paccache.timer
